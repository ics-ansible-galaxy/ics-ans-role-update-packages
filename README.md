ics-ans-role-update-packages
===================

Ansible role to update-packages. Uses [state: latest] thus [skip_ansible_lint] is used to ignore error [ANSIBLE0010].

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------

```yaml
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-update-packages
```

License
-------

BSD 2-clause
